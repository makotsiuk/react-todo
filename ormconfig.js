module.exports = {
    "type": "sqlite",
    "database": "src/server/database/db.sqlite",
    "entities":  process.env.NODE_ENV === 'test' ? 
        ["src/**/*.entity{.ts,.js}"]
        : ["dist/**/*.entity{.ts,.js}"],
    "migrations": [
        "migration/*{.ts,.js}"
    ],
    "seeds": [
        "src/server/database/seeds/**/*{.ts,.js}"],
    "factories": ["src/server/database/factories/**/*{.ts,.js}"],
    "synchronize": true
};