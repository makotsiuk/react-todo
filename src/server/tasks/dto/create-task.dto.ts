import { IsNotEmpty, Min, Max } from 'class-validator';
import { Status } from "../entities/task.entity";

export class CreateTaskDto {

    @IsNotEmpty()
    public title: string;

    @IsNotEmpty()
    public description: string;

    @Min(0)
    @Max(2)
    @IsNotEmpty()
    public status: Status;
}
