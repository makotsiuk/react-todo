import { PartialType } from '@nestjs/mapped-types';
import { IsNotEmpty, Min, Max } from 'class-validator';
import { CreateTaskDto } from './create-task.dto';
import { Status } from "../entities/task.entity";

export class UpdateTaskDto extends PartialType(CreateTaskDto) 
{
    @IsNotEmpty()
    public title: string;

    @IsNotEmpty()
    public description: string;

    @Min(0)
    @Max(2)
    @IsNotEmpty()
    public status: Status;
}
