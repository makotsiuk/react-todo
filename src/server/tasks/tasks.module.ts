import { ListRepository } from './../lists/list.repository';
import { TaskRepository } from './task.repository';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TasksService } from './tasks.service';
import { TasksController } from './tasks.controller';

@Module({
  imports: [TypeOrmModule.forFeature([TaskRepository, ListRepository])],
  controllers: [TasksController],
  providers: [TasksService]
})
export class TasksModule {}
