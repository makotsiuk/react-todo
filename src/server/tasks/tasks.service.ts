import { ListRepository } from '../lists/list.repository';
import { TaskRepository } from './task.repository';
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { Task } from './entities/task.entity';
import { List } from '../lists/entities/list.entity';

@Injectable()
export class TasksService {

  constructor(
    private readonly taskRepository: TaskRepository,
    private readonly listRepository: ListRepository
  ) {}

  async create(listId: number, createTaskDto: CreateTaskDto, userId: number) {

    const { 
        title,
        description,
        status
    } = createTaskDto;

    const list = await this.listRepository.findOne(listId, {
      relations: ['user']
    });

    if (!list || list.user.id !== userId) {
      throw new NotFoundException();
    }

    const task = new Task();

    task.title = title;
    task.description = description;
    task.status = status;
    task.list = list;
    
    return await this.taskRepository.save(task);
    
  }

  async findAll(listId: number, userId: number) {

    const list = await this.listRepository.findOne(listId, {
      relations: ['user', 'tasks']
    });

    if (!list || list.user.id !== userId) {
      throw new NotFoundException();
    }

    return list.tasks;
  }

  async findOne(listId: number, id: number, userId: number) {
    const list = await this.listRepository.findOne(listId, {
      relations: ['user']
    });

    if (!list || list.user.id !== userId) {
      throw new NotFoundException();
    }

    const task = await this.taskRepository.findOne(id, {
      relations: ['list']
    });

    if (!task || task.list.id !== listId) {
      throw new NotFoundException();
    }

    return task;
  }

  async update(listId: number, id: number, updateTaskDto: UpdateTaskDto, userId: number) {
    const { 
        title,
        description,
        status
    } = updateTaskDto;

    const list = await this.listRepository.findOne(listId, {
      relations: ['user']
    });

    if (!list || list.user.id !== userId) {
      throw new NotFoundException();
    }

    const task = await this.taskRepository.findOne(id, {
      relations: ['list']
    });

    if (!task || task.list.id !== listId) {
      throw new NotFoundException();
    }

    return (await this.taskRepository.save({
      ...task,
      title,
      description,
      status
    }));
  }

  async remove(listId: number, id: number, userId: number) {
    const list = await this.listRepository.findOne(listId, {
      relations: ['user']
    });

    if (!list || list.user.id !== userId) {
      throw new NotFoundException();
    }

    const task = await this.taskRepository.findOne(id, {
      relations: ['list']
    });

    if (!task || task.list.id !== listId) {
      throw new NotFoundException();
    }

    return (await this.taskRepository.delete(id)).affected;
  }
}
