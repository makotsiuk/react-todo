import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import { Controller, Get, Post, Body, Patch, Param, Delete, Request, UseGuards } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';

@UseGuards(JwtAuthGuard)
@Controller('/api/tasks')
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  @Post(':listId')
  create(@Param('listId') listId: number, @Body() createTaskDto: CreateTaskDto, @Request() req) {
    const { user } = req;
    return this.tasksService.create(+listId, createTaskDto, +user.id);
  }

  @Get(':listId')
  findAll(@Param('listId') listId: number, @Request() req) {
    const { user } = req;
    return this.tasksService.findAll(+listId, +user.id);
  }

  @Get(':listId/:id')
  findOne(@Param('listId') listId: number, @Param('id') id: number, @Request() req) {
    const { user } = req;
    return this.tasksService.findOne(+listId, id, +user.id);
  }

  @Patch(':listId/:id')
  update(@Param('listId') listId: number, @Param('id') id: string, @Body() updateTaskDto: UpdateTaskDto, @Request() req) {
    const { user } = req;
    return this.tasksService.update(+listId, +id, updateTaskDto, +user.id);
  }

  @Delete(':listId/:id')
  remove(@Param('listId') listId: number, @Request() req, @Param('id') id: string) {
    const { user } = req;
    return this.tasksService.remove(+listId, +id, +user.id);
  }
}
