import { List } from './../../lists/entities/list.entity';
import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne } from 'typeorm';

export enum Status {
    ToDo,
    InProgress,
    Done
}


@Entity({
    name: 'tasks'
})
export class Task extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column('text')
    description: string;


    @Column('int')
    status: Status;

    @Column({
        type: 'int'
    })
    created_at: Date;

    @ManyToOne(() => List, list => list.tasks, {
        onDelete: 'CASCADE'
    })
    list: List
}
