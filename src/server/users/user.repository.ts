import { AuthService } from './../auth/auth.service';
import { CreateUsertDTO } from './create-user.dto';
import { Repository, EntityRepository } from 'typeorm';
import { User } from './user.entity';

@EntityRepository(User)
export class UserRepository extends Repository<User> {

    constructor( 
    ) {
        super();
    }

    async createUser(
        createUserDTO: CreateUsertDTO
    ) {
        const { email, password } = createUserDTO;
        const user = new User();
        user.email = email;
        user.password = password;
        await user.save();
        return {
            status: 'ok'
        }
    }
}

