import { CreateUsertDTO } from './create-user.dto';
import { UsersService } from './users.service';
import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';

@Controller('/api/users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() createUsertDTO: CreateUsertDTO) {
    return this.usersService.createNew(createUsertDTO);
  }
}
