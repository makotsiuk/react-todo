import * as bcrypt from 'bcrypt';
import { List } from './../lists/entities/list.entity';
import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, OneToMany, BeforeInsert } from 'typeorm';

@Entity({
  name: 'users'
})
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    unique: true
  })
  email: string;

  @Column()
  password: string;

  @OneToMany(() => List, list => list.user, {
    cascade: true
  })
  lists: List[];

  @BeforeInsert()
  async setPassword(password: string) {
    const salt = await bcrypt.genSalt()
    this.password = await bcrypt.hash(password || this.password, salt)
  }

}