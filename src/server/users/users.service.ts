import { UserRepository } from './user.repository';
import { ConflictException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { CreateUsertDTO } from './create-user.dto';

@Injectable()
export class UsersService {
  constructor(
      @InjectRepository(User)
      private userRepository: UserRepository
  ) {}

  async findOne(email: string): Promise<User | undefined> {
    return this.userRepository.findOne({ email });
  }

  //DELETE
  async findAll(): Promise<User[] | undefined>{
      return this.userRepository.find();
  }

  async createNew(createUsertDTO: CreateUsertDTO) {
    const isUserExist = await this.userRepository.findOne({email: createUsertDTO.email});

    if (isUserExist) throw new ConflictException('User with this email already exists!');

    return this.userRepository.createUser(createUsertDTO);
  }
}