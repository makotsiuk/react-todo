import { Task } from './../../tasks/entities/task.entity';
import { List } from './../../lists/entities/list.entity';
import { User } from './../../users/user.entity';
import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';



export class DataSeeder implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<void> {
    let counter = 0;

    const users = await factory(User)()
      .map(async user => {
        user.email = `admin${counter ? counter : ''}@admin.com`;
        user.password = 'adminadmin';
        counter += 1;

       

        return user;
      })
      .createMany(2);

      for (const user of users) {
        const lists = await factory(List)() 
          .createMany(20, {user});

          for (const list of lists) {
            await factory(Task)().createMany(15, {list});
          }
      }      
  }
  
}
