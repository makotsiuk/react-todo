import Faker from 'faker'
import { define } from 'typeorm-seeding'
import { List } from '../../lists/entities/list.entity'


define(List, (faker: typeof Faker) => {
    const list = new List();
    list.title = faker.lorem.sentence();
    list.excerpt = faker.lorem.sentence();
    list.thumbnail = faker.image.imageUrl();
    list.created_at = faker.date.past();
    return list;
});