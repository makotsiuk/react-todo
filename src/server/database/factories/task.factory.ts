import Faker from 'faker'
import { define } from 'typeorm-seeding'
import { Task } from '../../tasks/entities/task.entity'


define(Task, (faker: typeof Faker) => {
    const task = new Task()
    task.title = faker.lorem.sentence();
    task.description = faker.lorem.text();
    task.created_at = faker.date.past();
    task.status = faker.random.number({
        min: 0,
        max: 2
    });
    return task;
});