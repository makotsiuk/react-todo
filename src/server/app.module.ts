import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { ListsModule } from './lists/lists.module';
import { TasksModule } from './tasks/tasks.module';
import { ViewModule } from './modules/view/view.module';


@Module({
  imports: [
    TypeOrmModule.forRoot(),
    AuthModule, 
    UsersModule, 
    ListsModule, 
    TasksModule,
    ViewModule
  ],
  controllers: [],
  providers: [],
})

export class AppModule {}
