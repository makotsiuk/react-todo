import { ListRepository } from './list.repository';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ListsService } from './lists.service';
import { ListsController } from './lists.controller';

@Module({
  imports: [TypeOrmModule.forFeature([ListRepository])],
  controllers: [ListsController],
  providers: [ListsService]
})
export class ListsModule {}
