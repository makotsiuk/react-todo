import { CreateListDto } from './dto/create-list.dto';
import { Repository, EntityRepository } from 'typeorm';
import { List } from './entities/list.entity';

@EntityRepository(List)
export class ListRepository extends Repository<List> {

}