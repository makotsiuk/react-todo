import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Request, Query } from '@nestjs/common';
import { ListsService } from './lists.service';
import { CreateListDto } from './dto/create-list.dto';
import { UpdateListDto } from './dto/update-list.dto';

@UseGuards(JwtAuthGuard)
@Controller('/api/lists')
export class ListsController {
  constructor(private readonly listsService: ListsService) {}

  @Post()
  create(@Body() createListDto: CreateListDto, @Request() req) {
    const { user } = req;
    
    return this.listsService.create(createListDto, user);
  }

  @Get()
  findAll(@Query('offset') offset: string, @Request() req) {
    const { user } = req;
    return this.listsService.findAll(+user.id, offset);
  }

  @Get(':id')
  findOne(@Param('id') id: string, @Request() req) {
    const { user } = req;
    return this.listsService.findOne(+id, +user.id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateListDto: UpdateListDto, @Request() req) {
    const { user } = req;
    return this.listsService.update(+id, updateListDto, +user.id);
  }

  @Delete(':id')
  remove(@Param('id') id: string, @Request() req) {
    const { user } = req;
    return this.listsService.remove(+id, +user.id);
  }
}
