import { Task } from './../../tasks/entities/task.entity';
import { User } from './../../users/user.entity';
import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, ManyToOne, OneToMany } from 'typeorm';

@Entity({
    name: 'lists'
})
export class List extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    thumbnail: string;

    @Column()
    excerpt: string;

    @Column({
        type: 'int'
    })
    created_at: Date;

    @ManyToOne(() => User, user => user.lists, {
        onDelete: 'CASCADE'
    })
    user: User;

    @OneToMany(() => Task, task => task.list, {
        cascade: true
    })
    tasks: Task[];
}
