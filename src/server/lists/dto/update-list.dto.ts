import { PartialType } from '@nestjs/mapped-types';
import { IsNotEmpty } from 'class-validator';
import { CreateListDto } from './create-list.dto';

export class UpdateListDto extends PartialType(CreateListDto) {
    @IsNotEmpty()
    title: string;

    @IsNotEmpty()
    thumbnail: string;

    @IsNotEmpty()
    excerpt: string;
}
