import { IsNotEmpty } from 'class-validator';
export class CreateListDto {
    @IsNotEmpty()
    title: string;

    @IsNotEmpty()
    thumbnail: string;

    @IsNotEmpty()
    excerpt: string;
}
