import { List } from './entities/list.entity';
import { ListRepository } from './list.repository';
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateListDto } from './dto/create-list.dto';
import { UpdateListDto } from './dto/update-list.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../users/user.entity';

@Injectable()
export class ListsService {

  constructor(
    @InjectRepository(List)
    private readonly listRepository: ListRepository
  ) {}

  async create(createListDto: CreateListDto, user: User) {
   const list = new List();
   list.title = createListDto.title;
   list.thumbnail = createListDto.thumbnail,
   list.excerpt = createListDto.excerpt
   list.created_at = new Date();
   list.user = user;
   return await this.listRepository.save(list);
  }

  async findAll(userId, offset) {

    const lists = await this.listRepository.findAndCount({
      where: {
        user: {
          id: userId
        }
      },
      take: 6,
      skip: offset,
      order: {
        created_at: 'DESC'
      }
    });

    return lists;
  }

  async findOne(id: number, userId: number) {
    
    const list: List = await this.listRepository.findOne(id, {
      where: {
        user: {
          id: userId
        }
      },
      relations: [
        'tasks'
      ]
    });

    return list;
  }

  async update(id: number, updateListDto: UpdateListDto, userId: number) {
    const list: List = await this.listRepository.findOne(id, {
      where: {
        user: {
          id: userId
        }
      }
    });
   

    if (list) {

      return (await this.listRepository.save({
        ...list,
        title: updateListDto.title,
        thumbnail: updateListDto.thumbnail,
        excerpt: updateListDto.excerpt
      }));
    } else {
      throw new NotFoundException();
    }
  }

  async remove(id: number, userId: number) {

    const list: List = await this.listRepository.findOne(id, {
      where: {
        user: {
          id: userId
        }
      }
    });

    if (list) {
      return (await this.listRepository.delete(id)).affected;
    } else {
      throw new NotFoundException();
    }
  }
}
