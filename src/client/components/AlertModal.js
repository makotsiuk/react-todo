import { Modal } from 'antd';
import React, { useContext } from 'react';
import AlertModalContext from '../contexts/AlertModalContext';

const AlertModal = () => {

    const { 
        modalMode, 
        visible, 
        setVisible, 
        deleteList, 
        deleteTask 
    } = useContext(AlertModalContext);

    return (
        <Modal
            title={`Delete ${modalMode === 'list' ? 'list' : 'task'}`}
            onOk={() => {
                setVisible(false);
                modalMode === 'list' ? deleteList() : deleteTask();
            }}
            okType="danger"
            onCancel={() => setVisible(false)}
            visible={visible}
        >
            <p>Are you sure you want to delete this {`${modalMode === 'list' ? 'list' : 'task'}`}?</p>
        </Modal>
    );
};

export default AlertModal;