import React, { useContext } from 'react';
import NextLink from 'next/link';
import { Collapse, Menu, Dropdown, Typography } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import AlertModalContext from '../../contexts/AlertModalContext';

const { Panel } = Collapse;

const { Link } = Typography;

const Task = ({ item }) => {

    const { setSelectedTask, setModalMode, setVisible } = useContext(AlertModalContext);

    const activeList = useSelector(state => state.activeList);

    const options = itemId => {

        const menu = (
            <Menu>
                <Menu.Item><NextLink href={`/list/${activeList.id}/edit/${itemId}`}><a>Edit task</a></NextLink></Menu.Item>
                <Menu.Item 
                    danger 
                    onClick={() => {
                        setSelectedTask(itemId);
                        setModalMode('task');
                        setVisible(true);
                    }}
                >Delete Task</Menu.Item>
            </Menu>
        );

        return (
            <Dropdown overlay={menu}>
                <Link className="ant-dropdown-link" onClick={e => {
                    e.preventDefault();
                    e.stopPropagation();
                }}>
                Options <DownOutlined />
                </Link>
            </Dropdown>
        );
    };

    return (
        <Collapse className="task">
            <Panel 
                header={<p>{item.title}</p>} 
                extra={options(item.id)}
            >
                <p>{item.description}</p>
            </Panel>
        </Collapse>
    )
};

export default Task;