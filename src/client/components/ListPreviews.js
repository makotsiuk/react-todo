import React, { useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

import { Row, Col, Button, Space, Breadcrumb, Pagination } from 'antd';

import { useDispatch, useSelector } from 'react-redux';

import { listsActions } from '../store/actions/index';
import PreviewCard from './preview-card/PreviewCard';

const ListPreviews = () => {

    const router = useRouter();

    const perPage = 6;

    const { pageNum } = router.query;

    const currentPage = pageNum ? +pageNum : 1;


    const [
        lists, 
        listFetched, 
        listsCount
    ] = useSelector(state => [
        state.lists.items, 
        state.lists.fetched, 
        state.lists.count
    ]);

    if (listFetched && (currentPage - 1) * perPage > listsCount) {

        router.push('/');
    }

   

    const dispatch = useDispatch();
      
    useEffect(() => {
        !listFetched && dispatch(listsActions.fetchLists((currentPage - 1) * perPage));
    }, [lists, dispatch, listFetched, currentPage]);

    const onPaginationChange = page => {
        const route = page === 1 ? '/' : '/page/' + page;
        router.push(route);
        dispatch(listsActions.fetchLists((page - 1) * perPage));
    };



    return (
         <Space
            size="middle"
            direction="vertical"
            style={{
                width: '100%',
                maxWidth: '1440px',
                margin: '0 auto'
            }}
        >
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link 
                        href="/" 
                    ><a>Home</a></Link>
                </Breadcrumb.Item>
            </Breadcrumb>
            <Button
                type="primary"
            >
                <Link
                    href="/list/new"
                ><a>Create new List</a></Link>
            </Button>
            <Row 
                gutter={[16, 16]} 
                justify="start"
            >
            { lists.length ?  lists.map(list => (
                <Col 
                    key={list.id}
                    span="24"
                    md={{span: 12}} 
                    lg={{span: 8}}
                >
                    <PreviewCard 
                        id={list.id}
                        title={list.title}
                        thumbnail={list.thumbnail}
                        excerpt={list.excerpt}
                        created_at={list.created_at}
                    />
                </Col>
            )) : <Col>You have no lists yet!</Col> }
            
            </Row>

            <Pagination 
                simple 
                current={currentPage} 
                defaultPageSize={perPage}
                total={listsCount ? listsCount : 1} 
                style={{
                    textAlign: 'center'
                }}
                onChange={onPaginationChange}
            />
         
        </Space>
    );
};

export default ListPreviews;