import React, { useEffect, useState } from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { Typography } from 'antd';
import { useSelector, useDispatch } from 'react-redux';


import Task from '../task/Task';
import c from './dnd-panel.module.scss';

import { activeListActions } from '../../store/actions/index';

const { Title } = Typography; 

const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
};

const move = (source, destination, droppableSource, droppableDestination) => {
    const sourceClone = Array.from(source);
    const destClone = Array.from(destination);
    const [removed] = sourceClone.splice(droppableSource.index, 1);

    destClone.splice(droppableDestination.index, 0, removed);

    const result = {};
    result[droppableSource.droppableId] = sourceClone;
    result[droppableDestination.droppableId] = destClone;

    return result;
};

const taskNums = {
    toDo: 0,
    inProgress: 1,
    finished: 2
}

const getTasks = activeList => {
    const obj = {
        toDo: [],
        inProgress: [],
        finished: [],
    };

    activeList.tasks.forEach(task => {
        let keyName = 'finished';
        if (0 === task.status) {
            keyName = 'toDo';
        } else if (1 === task.status) {
            keyName = 'inProgress';
        } 

        obj[keyName].push({
            ...task,
            id: String(task.id)
        });
    });

    return  obj;
};

const columnTitles = {
    finished: {
        type: 'success',
        text: 'Done'
    },
    toDo: {
        type: 'secondary',
        text: 'To Do'
    },
    inProgress: {
        type: 'warning',
        text: 'In Progress'
    }
}



const DragAndDropPanel = () => {

    const dispatch = useDispatch();

    const [activeList] = useSelector(state => [state.activeList]);

    const [tasks, setTasks] = useState(getTasks(activeList));

    useEffect(() => {
        setTasks(getTasks(activeList))
    }, [activeList]);

    const onDragEnd = result => {
        const { source, destination, draggableId } = result;

        if (!destination) {
            return;
        }

        if (source.droppableId === destination.droppableId) {
            const items = reorder(
                tasks[source.droppableId],
                source.index,
                destination.index
            );

            setTasks({
                ...tasks,
                [destination.droppableId]: items
            });
        
        } else {

            const result = move(
                tasks[source.droppableId],
                tasks[destination.droppableId],
                source,
                destination
            );

            setTasks({
                ...tasks,
                [source.droppableId]: result[source.droppableId],
                [destination.droppableId]: result[destination.droppableId]
            });
        }


        const targetItem = activeList.tasks.find(el => +draggableId === el.id);
        const newStatus =  taskNums[destination.droppableId];


        if (targetItem.status !== newStatus) {
            dispatch(activeListActions.editListItem(
                activeList.id, 
                targetItem.id, 
                {
                    ...targetItem,
                    status: newStatus
                }
                
            ));
        }
    };

    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <div className={c.panel}>
                { Object.keys(tasks).map(tasksKey => (
                    <Droppable
                        key={tasksKey} 
                        droppableId={tasksKey}
                    >
                        {(provided, snapshot) => (
                            <div
                                ref={provided.innerRef}
                                className={`${c.column} ${ snapshot.isDraggingOver && c.columnDraggedOver}`}
                            >
                                <Title level={2} type={ columnTitles[tasksKey].type}>{ columnTitles[tasksKey].text }</Title>
                                {tasks[tasksKey].map((item, index) => (
                                    <Draggable
                                        key={item.id}
                                        draggableId={item.id}
                                        index={index}>
                                        {(provided, snapshot) => (
                                            <div
                                                ref={provided.innerRef}
                                                {...provided.draggableProps}
                                                {...provided.dragHandleProps}
                                                className={`
                                                    ${c.dragItem} 
                                                    ${snapshot.isDragging && c.dragItemIsDragging}
                                                `}
                                                style={provided.draggableProps.style}
                                                >
                                                <Task
                                                    item={item}
                                                />
                                            </div>
                                        )}
                                    </Draggable>
                                ))}
                                {provided.placeholder}
                            </div>
                        )}
                    </Droppable>
                ))}
            </div>
        </DragDropContext>
    )
}

export default DragAndDropPanel;