import React, { useState } from 'react';
import  { useRouter } from 'next/router';

import { Card, Spin, Alert } from 'antd';
import moment from 'moment';
import c from './preview-card.module.scss';


const PreviewCard = ({ id, title, excerpt, thumbnail, created_at }) => {


    const router = useRouter();
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    return (
        <Card                        
            title={title}
            style={{
                height: '100%'
            }}
            hoverable
            cover={
                <div className={c.previewPanel}>
                    { loading &&  (
                        <div className={c.previewPanelLoader}>
                            <Spin />
                        </div>
                    )}
                    
                    {!error && <img 
                        alt={title} 
                        src={thumbnail + '?random=' + id} 
                        onLoad={() => setLoading(false)}
                        onError={() => {
                            setLoading(false);
                            setError(true);
                        }}
                    />}

                    {error && <div className={c.previewPanelError}>
                        <Alert
                            message="Error"
                            description="Failed to load image"
                            type="error"
                            showIcon
                        />
                        </div>
                    }
                </div>
            }
            onClick={() => router.push(`/list/${id}`)}
        >
            <Card.Meta 
                title={excerpt} 
                description={moment(created_at).format('LL')}
            />
        </Card>
    ); 
};

export default PreviewCard;