import ListPreviews from "../components/ListPreviews";
import WithAuth from '../HOC/WithAuth';

const HomePage = () => <ListPreviews />;

export const getServerSideProps = WithAuth();

export default HomePage;