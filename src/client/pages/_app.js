import "antd/dist/antd.css";
import '../index.css';



import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import MainLayout from '../layouts/MainLayout';


import authReducer from '../store/slices/auth';
import listsReducer from '../store/slices/lists';
import activeListReducer from '../store/slices/activeList';


const store = configureStore({
    reducer: {
        auth: authReducer,
        lists: listsReducer,
        activeList: activeListReducer
    }
});



  
const App = ({ Component, pageProps }) => (
    <Provider store={store}>   
        <MainLayout>
            <Component {...pageProps} />
        </MainLayout>      
    </Provider>
);


export default App;