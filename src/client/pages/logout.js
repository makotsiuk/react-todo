import { useEffect } from 'react';
import { useRouter } from 'next/router';

import { useDispatch } from 'react-redux';

import * as actions from '../store/actions/auth';

const Logout = () => {
    const dispatch = useDispatch()
    const router =  useRouter();

    useEffect(() => {
      actions.endSession(dispatch);
      router.push('/auth');
    }, [dispatch]);


    return null;
};

  

export default Logout;

