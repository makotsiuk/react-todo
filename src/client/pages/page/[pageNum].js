import ListPreviews from "../../components/ListPreviews";
import WithAuth from '../../HOC/WithAuth';

const PreviewPage = () => <ListPreviews />;

export const getServerSideProps = WithAuth();

export default PreviewPage;