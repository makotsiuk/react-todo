import { useRouter } from 'next/router';
import {useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Row, Col, Typography, Form, Input, Button } from 'antd';
import * as actions from '../store/actions/auth';
import WithAuth from '../HOC/WithAuth';

const { Title, Link, Text } = Typography;


const Auth = () => {

    const router = useRouter();

    const [signupMode, setSignupMode] = useState(false);

    const error = useSelector(state => state.auth.error);

    const dispatch = useDispatch();

    const onFinish = values => {
        const { email, password} = values; 
        dispatch(actions.auth(email, password, signupMode, router));
    };


    const getErrorMessage = text => {
        const hasDivder = text.indexOf(':');

        if (hasDivder > -1) {
            return text.slice(hasDivder + 1, text.length).trim();
        }

        return text;

    };


    return (
        <Row>
            <Col xs={{span: 24}} md={{span: 12, offset: 6}}>
                <Title>{ signupMode ? 'Please create an account!' : 'Please login to your account!' }</Title>

                <p>{ signupMode ? 'Already have an account?' : 'Don\'t have an account?' } <Link onClick={() => setSignupMode(!signupMode)}>{ signupMode ? 'Log In' : 'Sing Up' }</Link></p> 
                {error && <p><Text type="danger">{ getErrorMessage(error.message) }</Text></p>} 
                <Form
                    layout="vertical"
                    onFinish={onFinish}
                    noValidate
                >
                    <Form.Item
                        label="Email"
                        name="email"
                        rules={[
                            { 
                                required: true, 
                                message: 'Please enter your email!' 
                            },
                            {
                                type: 'email',
                                message: 'Please enter valid email address!' 
                            }
                        ]}
                    >
                        <Input type="email" />
                    </Form.Item>
                    <Form.Item
                        label="Password"
                        name="password"
                        rules={[
                            {
                                required: true, 
                                message: 'Please enter your password!' 
                            },
                            {
                                min: 6,
                                message: 'Password is too short!' 
                            }
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>
                    {         
                        signupMode &&  <Form.Item
                            label="Repeat password"
                            name="repeat_password"
                            dependencies={['password']}
                            rules={[
                                {
                                    required: true, 
                                    message: 'Please repeat your password!' 
                                },
                                ({ getFieldValue }) => ({
                                    validator(_, value) {
                                        if (!value || getFieldValue('password') === value) {
                                          return Promise.resolve();
                                        }
                                        return Promise.reject(new Error('The two passwords that you entered do not match!'));
                                      },
                                })
                            ]}
                        >
                            <Input.Password />
                        </Form.Item>
                    }
                    <Form.Item>
                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Col>
        </Row>
    );
};

export const getServerSideProps = WithAuth();

export default Auth;