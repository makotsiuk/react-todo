import Link from 'next/link';
import { useRouter } from 'next/router';

import { useDispatch } from 'react-redux';
import { activeListActions } from '../../store/actions';
import { Form, Input, Button, Typography, Row, Col, Breadcrumb, Space } from 'antd';


const { Title } = Typography;


const CreateForm = () => {

    const dispatch = useDispatch();

    const router = useRouter();

    const onFinish = values => {
        dispatch(activeListActions.createList(values, router));
    };


    return (
        <Space
            size="middle"
            direction="vertical"
            style={{
                width: '100%'
            }}
        >
            <Breadcrumb>
                <Breadcrumb.Item><Link href="/"><a>Home</a></Link></Breadcrumb.Item>
                <Breadcrumb.Item>Create a new list</Breadcrumb.Item>
            </Breadcrumb>
            <Row justify="start">
             
                <Col xs={{span: 24}} md={{span: 12}}>
                    <Title>Create a new list</Title>
                    <Form
                        layout="vertical"
                        onFinish={onFinish}
                        noValidate
                    >
                        <Form.Item
                            label="Title"
                            name="title"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Please enter list title!' 
                                }
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="Short description"
                            name="excerpt"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Please enter short description!' 
                                }
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="Thumbnail"
                            name="thumbnail"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Please enter thumbnail!' 
                                }
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        
                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                            Create New List
                            </Button>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </Space>
    );
}

export default CreateForm;