import React, { useEffect } from 'react';
import { useRouter } from 'next/router';

import { useDispatch, useSelector } from 'react-redux'
import Link from 'next/link';



import { activeListActions } from '../../../../store/actions/index';

import { Form, Input, Button, Typography, Row, Col, Space, Breadcrumb } from 'antd';

const { Title } = Typography;


const EditForm = () => {

    const router = useRouter();

    const { id } = router.query;

    const [activeList] = useSelector(state => [state.activeList]);

    const dispatch = useDispatch();

    

    const [form] = Form.useForm();

    

    useEffect(() => {
        +id !== activeList.id && dispatch(activeListActions.fetchList(id, router));
    }, [id, activeList, dispatch, router]);

    const { title, excerpt, thumbnail } = activeList;

    form.setFieldsValue({
        title,
        thumbnail,
        excerpt
    });

    const onFinish = values => {
        dispatch(activeListActions.editList(id, values, router))
    };

    return (
        <Space direction="vertical" size="middle" style={{width: '100%'}}>
             <Breadcrumb>
                <Breadcrumb.Item><Link href="/"><a>Home</a></Link></Breadcrumb.Item>
                <Breadcrumb.Item><Link href={`/list/${id}`}><a>{ title }</a></Link></Breadcrumb.Item>
                <Breadcrumb.Item>Edit list title</Breadcrumb.Item>
            </Breadcrumb>
            <Row>
                <Col xs={{span: 24}} md={{span: 12}}>
                    <Title>Edit list title</Title>
                    <Form
                        layout="vertical"
                        onFinish={onFinish}
                        initialValues={{
                            title,
                            excerpt,
                            thumbnail
                        }}
                        noValidate
                        form={form}
                    >
                        <Form.Item
                            label="Title"
                            name="title"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Please enter list title!' 
                                }
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="Shot description"
                            name="excerpt"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Please enter short description!' 
                                }
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="Thumbnail"
                            name="thumbnail"
                            rules={[
                                { 
                                    required: true, 
                                    message: 'Please enter thumbnail!' 
                                }
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        
                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                            Edit List
                            </Button>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
        </Space>
    );
}



export default EditForm;