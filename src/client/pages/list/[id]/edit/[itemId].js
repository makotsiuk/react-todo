import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';


import { Form, Input, Select, Button, Space, Breadcrumb } from 'antd';
import { useDispatch, useSelector } from 'react-redux';

import { activeListActions } from '../../../../store/actions/index';


const { Option } = Select;

const EditListItemForm = () => {

    const router = useRouter();

    const { id, itemId } = router.query;

    const [form] = Form.useForm();

    const [activeList] = useSelector(state => [state.activeList]);

    const dispatch = useDispatch();

    

    useEffect(() => {
        +id !== activeList.id && dispatch(activeListActions.fetchList(id, router));
    }, [id, activeList, dispatch, router]);

    const targetItem = activeList.tasks.length ? 
        activeList.tasks[activeList.tasks.findIndex(el => el.id === +itemId)]
        :
        {
            title: '',
            description: '',
            status: 0
        };

    const { title, description, status } = targetItem;

    form.setFieldsValue({
        title, 
        description,
        status
    });

    const onFinish = values => {
        dispatch(activeListActions.editListItem(id, itemId, values, router));
    };

    const statusOptions = ['To Do', 'In Progress', 'Done'];
    
    return (
        <Space direction="vertical" size="middle" style={{width: '100%'}}>
            <Breadcrumb>
                <Breadcrumb.Item><Link href="/"><a>Home</a></Link></Breadcrumb.Item>
                <Breadcrumb.Item><Link href={`/list/${id}`}><a>{ activeList.title }</a></Link></Breadcrumb.Item>
                <Breadcrumb.Item>Edit task "{ title }"</Breadcrumb.Item>
            </Breadcrumb>
            <Form
                layout="vertical"
                onFinish={onFinish}
                noValidate
                form={form}
            >
                <Form.Item
                    label="Heading"
                    name="title"
                    rules={[
                        { 
                            required: true, 
                            message: 'Please enter task heading!' 
                        }
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Description"
                    name="description"
                    rules={[
                        {
                            required: true, 
                            message: 'Please enter task description!' 
                        },
                        {
                            min: 6,
                            message: 'Description is too short' 
                        }
                    ]}
                >
                    <Input.TextArea  />
                </Form.Item>
                <Form.Item
                    label="Status"
                    name="status"
                    rules={[
                        {
                            required: true, 
                            message: 'Select task status' 
                        }
                    ]}
                >
                    <Select>
                        {statusOptions.map((option, index) => <Option key={index} value={index}>{option}</Option>)}                        
                    </Select>
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </Space>
    );
}

export default EditListItemForm;