import {useContext, useEffect, useState} from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

import { Typography, Button, Space, Breadcrumb } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import AlertModal from '../../../components/AlertModal';
import AlertModalContext from '../../../contexts/AlertModalContext';
import DragAndDropPanel from '../../../components/drag-and-drop-panel/DragAndDropPanel';
import { activeListActions } from '../../../store/actions';

const { Text, Title } = Typography;

const SingleList = () => {

    const router = useRouter();

    const { id } = router.query;

    const [activeList] = useSelector(state => [state.activeList]);

    const dispatch = useDispatch();

    const { title, tasks, excerpt } = activeList;

    useEffect(() => {
       +id !== activeList.id && dispatch(activeListActions.fetchList(id, router));     
    }, [id, activeList, dispatch, router]);

   const [alertModalVisible, setAlertModalVisible] = useState(false);
   const [selectedTask, setSelectedTask] = useState(null);
   const [modalMode, setModalMode] = useState('list');

    const deleteList = () => {
        dispatch(activeListActions.deleteList(activeList.id,  router));
    };

    const deleteTask = () => {
        dispatch(activeListActions.deleteListItem(activeList.id, selectedTask));
    };

    const contextValue = useContext(AlertModalContext);

    return (
        <AlertModalContext.Provider value={{
            ...contextValue,
            modalMode,
            setModalMode,
            visible: alertModalVisible,
            setVisible: setAlertModalVisible,
            setSelectedTask,
            deleteList,
            deleteTask
        }}>
            <Space direction="vertical" size="middle" style={{width: '100%'}}>
                <Breadcrumb>
                    <Breadcrumb.Item><Link href="/"><a>Home</a></Link></Breadcrumb.Item>
                    <Breadcrumb.Item>{ title }</Breadcrumb.Item>
                </Breadcrumb>
                <Title style={{marginBottom: 0}}>{ title }</Title>
                <Text>{ excerpt }</Text>
                
                <Space>
                    <Button
                        type="primary"
                    >
                        <Link
                            href={`/list/${id}/edit`}
                        ><a>Edit List</a></Link>
                    </Button>
                    <Button
                        type="primary"
                    >
                        <Link
                            href={`/list/${id}/create-item`}
                        ><a>Add New Item</a></Link>
                    </Button>

                    <Button
                        type="danger"
                        onClick={() => {
                            setModalMode('list');
                            setAlertModalVisible(true);
                        }}
                    >Delete list</Button>
                </Space>

                { tasks.length ? 
                    <DragAndDropPanel />
                :
                    <p>This list has no items yet...</p>
                }

                <AlertModal />
            </Space>
        </AlertModalContext.Provider>
        
    );
}
  

export default SingleList;