import React, { useEffect } from 'react';

import Link from 'next/link';
import { useRouter } from 'next/router';

import { Form, Input, Select, Button, Breadcrumb, Space } from 'antd';
import { useDispatch, useSelector } from 'react-redux';


import { activeListActions } from '../../../store/actions/index';


const { Option } = Select;

const EditListItemForm = () => {

    const [activeList] = useSelector(state => [state.activeList]);

    const dispatch = useDispatch();

    const router = useRouter();

    const { id } = router.query;

    

    useEffect(() => {
        +id !== activeList.id && dispatch(activeListActions.fetchList(id, router));
    }, [id, activeList, dispatch, router]);

    const onFinish = values => {
        dispatch(activeListActions.createListItem(id, values, router));
    };

    return (
        <Space direction="vertical" size="middle" style={{width: '100%'}}>
            <Breadcrumb>
                <Breadcrumb.Item><Link href="/"><a>Home</a></Link></Breadcrumb.Item>
                <Breadcrumb.Item><Link href={`/list/${id}`}>{ activeList.title }</Link></Breadcrumb.Item>
                <Breadcrumb.Item>Add new item to the list</Breadcrumb.Item>
            </Breadcrumb>
            <Form
                layout="vertical"
                onFinish={onFinish}
                noValidate
            >
                <Form.Item
                    label="Heading"
                    name="title"
                    rules={[
                        { 
                            required: true, 
                            message: 'Please enter task heading!' 
                        }
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Description"
                    name="description"
                    rules={[
                        {
                            required: true, 
                            message: 'Please enter task description!' 
                        },
                        {
                            min: 6,
                            message: 'Description is too short' 
                        }
                    ]}
                >
                    <Input.TextArea  />
                </Form.Item>
                <Form.Item
                    label="Status"
                    name="status"
                    rules={[
                        {
                            required: true, 
                            message: 'Select task status' 
                        }
                    ]}
                >
                        <Select>
                        <Option value="0">To Do</Option>
                        <Option value="1">In Progress</Option>
                        <Option value="2">Done</Option>
                    </Select>
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Add New Item
                    </Button>
                </Form.Item>
            </Form>
        </Space>
    );
}




export default EditListItemForm;