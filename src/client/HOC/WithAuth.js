import Cookies from 'cookies';

const WithAuth = getServerSideProps => async (context) => {
    const {req, res, resolvedUrl } = context;
    const cookies = new Cookies(req, res);
    const token = cookies.get('token');



    if (!token && resolvedUrl !== '/auth') {
        res.writeHead(302, { Location: '/auth' });
        res.end();
    } else if (token && resolvedUrl == '/auth') {
        res.writeHead(302, { Location: '/' });
        res.end();
    }


    return getServerSideProps ? await getServerSideProps(context) : {
        props: {}
    };
    
}

export default WithAuth;
