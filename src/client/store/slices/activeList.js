import { createSlice } from '@reduxjs/toolkit';

export const slice = createSlice({
    name: 'lists',
    initialState:  {
        id: '',
        title: '',
        excerpt: '',
        created_at: '',
        thumbnail: '',
        tasks: []
    },
    reducers: {
        setActiveList: (state, { payload }) => {
            const { id, title, excerpt, created_at, tasks, thumbnail } = payload;
            state.id = id;
            state.thumbnail = thumbnail;
            state.title = title;
            state.excerpt = excerpt;
            state.created_at = created_at;
            state.tasks = tasks ? tasks.reverse() : [];
        },
        unsetActiveList: state => {
            state.id = '';
            state.title = '';
            state.excerpt = '';
            state.created_at = '';
            state.taks = [];
            state.fetched = false;
        },
        editList: (state, { payload }) => {
            const { title, excerpt, thumbnail } = payload;
            state.title = title;
            state.excerpt = excerpt;
            state.thumbnail = thumbnail;
    
        },
        createListItem: (state, { payload }) => {
            const { listItem } = payload;

            state.tasks.unshift(listItem);
        },
        editListItem: (state, { payload }) => {
            const { itemId, title, status, description } = payload;    
                    
            const targetTaskIndex = state.tasks.findIndex(el => el.id === +itemId);

            state.tasks[targetTaskIndex].title = title;
            state.tasks[targetTaskIndex].description = description;
            state.tasks[targetTaskIndex].status = status;
        },
        deleteListItem: (state, { payload }) => {
            const { listItemId } = payload;
    
            const targetTaskIndex = state.tasks.findIndex(el => el.id === +listItemId);

            state.tasks.splice(targetTaskIndex,1);
        }
    }
});

export const { 
    setActiveList,
    unsetActiveList,
    editList,
    createListItem,
    editListItem,
    deleteListItem
} = slice.actions;

export default slice.reducer;