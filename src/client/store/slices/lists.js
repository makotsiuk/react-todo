import { createSlice } from '@reduxjs/toolkit';

export const slice = createSlice({
    name: 'lists',
    initialState:  {
        items: [],
        fetched: false,
        count: 0
    },
    reducers: {
        setLists: (state, { payload }) => {
            const { lists, listCount } = payload;
            state.items = lists;
            state.fetched = true;
            state.count = listCount;
        },
        unsetLists: state => {
           state.items = [];
           state.fetched = false;
           state.count = 0;
        }
    }
});


export const { 
    setLists,
    unsetLists
} = slice.actions;

export default slice.reducer;