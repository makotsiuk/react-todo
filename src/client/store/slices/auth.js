import { createSlice } from '@reduxjs/toolkit';

export const slice = createSlice({
    name: 'auth',
    initialState: {
        token: null,
        userId: null,
        loading: false
    },
    reducers: {
        start: state => {
            state.loading = true;
        },
        success: (state, { payload }) => {
            const {token, userId} = payload;
            state.loading = false;
            state.token = token;
            state.userId = userId;
        },
        fail: state => {
            state.loading = false;
        },
        logout: state => {
            state.token = null;
            state.userId = null;
            state.loading = false;
        }
    }
});


export const { start, success, fail, logout } = slice.actions;

export default slice.reducer;