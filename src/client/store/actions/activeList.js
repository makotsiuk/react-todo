import axios from '../../axios';

import {
    setActiveList,
    unsetActiveList,
    editList as editListReducer,
    createListItem as createListItemReducer,
    editListItem as editListItemReducer,
    deleteListItem as deleteListItemReducer
} from '../slices/activeList';

import { fetchLists } from './lists';

import { unsetLists } from '../slices/lists';

import { message } from 'antd';

export const createListItem = (listId, params, history) => {

    params.status = +params.status;

    return async dispatch => {

        try {
            const response =  await axios.post(`/tasks/${listId}`, params, {
                requiresAuth: true
            });

            dispatch(createListItemReducer({
                listId,
                listItem: {
                    id: response.data.id,
                    ...params
                }
            }));
            
            history.push('/list/' + listId);
            message.success('New list item successfully created');

        } catch (err) {
            message.error('Failed to create list item!');
            history.push('/');
        }
    }

};

export const editListItem = (listId, itemId, params, history) => {
    return async dispatch => {

        try {
            await axios
                .patch(`/tasks/${listId}/${itemId}`, {
                    ...params
                }, {
                    requiresAuth: true
                });
            
            history && history.push('/list/' + listId);

            dispatch(editListItemReducer({
                listId,
                itemId,
                ...params
            }));

            message.success('List item successfully changed');
        } catch (err) {
            message.error('Failed to change list item!');
        }
    }

};


export const editList = (id, values, history) => {
    
    const { title, excerpt, thumbnail } = values;

    return async dispatch => {


        try {
            await axios
                .patch(`/lists/${id}`, {
                    title,
                    excerpt,
                    thumbnail
                }, {
                    requiresAuth: true
                });
                dispatch(editListReducer({
                    title,
                    excerpt,
                    thumbnail
                }));

                message.success('List title successfully changed!');
                history.push('/list/' + id);
        } catch (err) {
            message.error('Failed to change list title!');
        }
    };
};

export const fetchList = (id, history) => {

    return async dispatch => {
        try {
            const response = await axios
                .get(`/lists/${id}`, {
                    requiresAuth: true
                });
            
                if (response.data) {
                    dispatch(setActiveList(response.data));
                } else {
                    message.error('List not found!');
                    history.push('/');
                }

        } catch (err) {
            message.error('Failed fetching list!');
            history.push('/');
        }
    }
}

export const createList = (params, history) => {
    const { title, excerpt, thumbnail } = params; 
    
    return async dispatch => {
        try {
            const response = await axios
                .post('/lists', {
                    title,
                    excerpt, 
                    thumbnail
                }, {
                    requiresAuth: true
                });

                message.success('List successfully created');

                dispatch(setActiveList(response.data));
    
                history.push('/list/' + response.data.id);

                dispatch(unsetLists());
        } catch (err) {
            message.error('Unable to create list');
        }
    }
};

export const deleteList = (listId, history) => {
    return async dispatch => {
        try {
            await axios
                .delete(`/lists/${listId}`, {
                    requiresAuth: true
                });
                history.push('/');
                dispatch(unsetActiveList());
                dispatch(fetchLists(0));

                message.success('List successfully deleted!');
        } catch (err) {
            message.error('Failed deleting list!');
        }
        history.push('/');
    };
};


export const deleteListItem = (listId, listItemId) => {
    return async dispatch => {

        try {
            await axios
                .delete(`/tasks/${listId}/${listItemId}`, {
                    requiresAuth: true
                });
            
            dispatch(deleteListItemReducer({listId, listItemId}));

            message.success('Task successfully deleted!');

        } catch (err) {
            message.error('Failed deleting task!');
        }
    };
};