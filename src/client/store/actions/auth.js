import axios from '../../axios';
import Cookies from 'js-cookie';

import { start, success, fail, logout } from '../slices/auth';
import { unsetLists } from '../slices/lists';

import { message } from 'antd';



const toggleStorageData = (...args) => {
    const keys = ['token', 'expirationDate', 'userId'];

    keys.forEach((el, i) => {
        if (args.length) {
            Cookies.set(el, args[i]);
        } else {
            Cookies.remove(el);
        }
    });
}

export const endSession = dispatch => {
    toggleStorageData();
    dispatch(logout());
    dispatch(unsetLists());
};

export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
            endSession(dispatch);
        }, expirationTime * 1000);
    };
};

export const auth = (email, password, signupMode, router) => {
    return async dispatch => {

        dispatch(start());

        const data = {
            email,
            password
        };

        const login = async () => {
            try {
                const response = await axios.post('/auth/login', data, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });
                const { access_token, id } = response.data;
                const expirationDate = new Date(new Date().getTime() + 600 * 1000);
                toggleStorageData(access_token, expirationDate, id);
                dispatch(success({
                    token: access_token, 
                    userId: id
                }));
                // TTD: implement refresh token
                dispatch(checkAuthTimeout(600));
                router.push('/');
            } catch (err) {
                const code = err.response.data.statusCode;
                let erroMsg = '';
                switch (code) {
                    case 401:
                        erroMsg = 'Unknown username or password.';
                        break;
                    default:
                        erroMsg = 'Unknown error occured. Please try again.';
                }
                dispatch(fail());
                message.error(erroMsg);
            }
        };


        if (signupMode) {
            try {
                await axios.post('/users', data, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });
                login();
            } catch (err) {
                const code = err.response.data.statusCode;
                let erroMsg = '';
                switch (code) {
                    case 409: 
                        erroMsg = 'User with this email alreay exists.';
                        break;
                    default:
                        erroMsg = 'Unknown error occured. Please try again.';
                }
                dispatch(fail());
                message.error(erroMsg);
            }
        } else {
            login();
        }

        
    };
};

export const check = () => {
    return dispatch => {
        const token = Cookies.get('token');
        if (!token) {
            dispatch(logout());
        } else {
            const expirationDate = new Date(Cookies.get('expirationDate'));
            if (expirationDate <= new Date()) {
                dispatch(logout());
            } else {
                const userId = Cookies.get('userId');
                dispatch(success({token: token, userId: userId}));
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000 ));
            }   
        }
    };
};
