import axios from '../../axios';

import {
    setLists
} from '../slices/lists';
import { message } from 'antd';

export const fetchLists = offset => {
    return async dispatch => {
        try {
            const response = await axios
                .get(`/lists?offset=${offset}`, {
                    requiresAuth: true
                });

            const lists =  response.data[0];
            const listCount = response.data[1];

            dispatch(setLists({lists, listCount}));

        } catch (err) {
            message.error('Failed fetching lists!');
        }
    }
};