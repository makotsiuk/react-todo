import { createContext } from 'react';

const AlertModalContext = createContext({
    modalMode: 'list',
    selectedTask: null,
    visible: false,
    setModalMode: () => {},
    setSelectedTask: () => {},
    setVisible: () => {},
    deleteList: () => {},
    deleteTask: () => {}
});

export default AlertModalContext;