import { useEffect, useState } from 'react';
import Link from 'next/link';

import { useDispatch, useSelector } from 'react-redux';
import { Layout, Menu } from 'antd';
import { FormOutlined } from '@ant-design/icons';

import { authActions } from '../store/actions/index';


const MainLayout = ( { children } ) => {

  const authenticated = useSelector(state => state.auth.token !== null);

  const [isLoaded, setIsLoaded] = useState(false);


  const dispatch = useDispatch();

  const { Header, Content, Footer } = Layout;

  useEffect(() => {
    dispatch(authActions.check());
    setIsLoaded(true);
  }, [dispatch]);

  return (
    <Layout style={{
      minHeight: '100vh',
      background: '#fff'
     }}>
      <Header
        style={{
          background: '#fff'
        }}
       >
        <Menu mode="horizontal">
          {
            authenticated ?
              (
                <>
                  <Menu.Item key="lists" icon={<FormOutlined />}>
                    <Link
                      href="/"
                    ><a>My lists</a></Link>
                  </Menu.Item>
                  <Menu.Item key="logout">
                    <Link
                      href="/logout"
                    ><a>Logout</a></Link>
                  </Menu.Item>
                </>
              )
              :
              (
                <Menu.Item key="auth">
                  <Link
                    href="/auth"
                  ><a>Login/Register</a></Link>
                </Menu.Item>
              )
          }
        </Menu>
      </Header>

      <Content style={{ padding: '20px 50px' }}>
        <div className="site-layout-content">
          { isLoaded && children }
        </div>
      </Content>
      <Footer><strong>Todo lists</strong> &copy;{ new Date().getFullYear() }</Footer>
    </Layout>
  );

};



export default MainLayout;