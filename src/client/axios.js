import axios from 'axios';
import Cookies from 'js-cookie';
import { BACKEND_URL } from './config';

const instance = axios.create({
    baseURL: BACKEND_URL
});

const getToken = () => {
    return Cookies.get('token');
};

instance.interceptors.request.use(config => {
    
    if (config.requiresAuth) {
        config.headers.Authorization =  'Bearer ' + getToken();
    } 

    return config
}, (error) => {
    console.log("Interceptor Request Error" + error)
})

export default instance;