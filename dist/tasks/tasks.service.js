"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TasksService = void 0;
const list_repository_1 = require("../lists/list.repository");
const task_repository_1 = require("./task.repository");
const common_1 = require("@nestjs/common");
const task_entity_1 = require("./entities/task.entity");
let TasksService = class TasksService {
    constructor(taskRepository, listRepository) {
        this.taskRepository = taskRepository;
        this.listRepository = listRepository;
    }
    async create(listId, createTaskDto, userId) {
        const { title, description, status } = createTaskDto;
        const list = await this.listRepository.findOne(listId, {
            relations: ['user']
        });
        if (!list || list.user.id !== userId) {
            throw new common_1.NotFoundException();
        }
        const task = new task_entity_1.Task();
        task.title = title;
        task.description = description;
        task.status = status;
        task.list = list;
        return await this.taskRepository.save(task);
    }
    async findAll(listId, userId) {
        const list = await this.listRepository.findOne(listId, {
            relations: ['user', 'tasks']
        });
        if (!list || list.user.id !== userId) {
            throw new common_1.NotFoundException();
        }
        return list.tasks;
    }
    async findOne(listId, id, userId) {
        const list = await this.listRepository.findOne(listId, {
            relations: ['user']
        });
        if (!list || list.user.id !== userId) {
            throw new common_1.NotFoundException();
        }
        const task = await this.taskRepository.findOne(id, {
            relations: ['list']
        });
        if (!task || task.list.id !== listId) {
            throw new common_1.NotFoundException();
        }
        return task;
    }
    async update(listId, id, updateTaskDto, userId) {
        const { title, description, status } = updateTaskDto;
        const list = await this.listRepository.findOne(listId, {
            relations: ['user']
        });
        if (!list || list.user.id !== userId) {
            throw new common_1.NotFoundException();
        }
        const task = await this.taskRepository.findOne(id, {
            relations: ['list']
        });
        if (!task || task.list.id !== listId) {
            throw new common_1.NotFoundException();
        }
        return (await this.taskRepository.save(Object.assign(Object.assign({}, task), { title,
            description,
            status })));
    }
    async remove(listId, id, userId) {
        const list = await this.listRepository.findOne(listId, {
            relations: ['user']
        });
        if (!list || list.user.id !== userId) {
            throw new common_1.NotFoundException();
        }
        const task = await this.taskRepository.findOne(id, {
            relations: ['list']
        });
        if (!task || task.list.id !== listId) {
            throw new common_1.NotFoundException();
        }
        return (await this.taskRepository.delete(id)).affected;
    }
};
TasksService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [task_repository_1.TaskRepository,
        list_repository_1.ListRepository])
], TasksService);
exports.TasksService = TasksService;
//# sourceMappingURL=tasks.service.js.map