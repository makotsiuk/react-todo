import { Repository } from 'typeorm';
import { Task } from './entities/task.entity';
export declare class TaskRepository extends Repository<Task> {
}
