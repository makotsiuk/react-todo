import { List } from './../../lists/entities/list.entity';
import { BaseEntity } from 'typeorm';
export declare enum Status {
    ToDo = 0,
    InProgress = 1,
    Done = 2
}
export declare class Task extends BaseEntity {
    id: number;
    title: string;
    description: string;
    status: Status;
    created_at: Date;
    list: List;
}
