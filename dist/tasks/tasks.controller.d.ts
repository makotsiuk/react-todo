import { TasksService } from './tasks.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
export declare class TasksController {
    private readonly tasksService;
    constructor(tasksService: TasksService);
    create(listId: number, createTaskDto: CreateTaskDto, req: any): Promise<import("./entities/task.entity").Task>;
    findAll(listId: number, req: any): Promise<import("./entities/task.entity").Task[]>;
    findOne(listId: number, id: number, req: any): Promise<import("./entities/task.entity").Task>;
    update(listId: number, id: string, updateTaskDto: UpdateTaskDto, req: any): Promise<{
        title: string;
        description: string;
        status: import("./entities/task.entity").Status;
        id: number;
        created_at: Date;
        list: import("../lists/entities/list.entity").List;
    } & import("./entities/task.entity").Task>;
    remove(listId: number, req: any, id: string): Promise<number>;
}
