import { ListRepository } from '../lists/list.repository';
import { TaskRepository } from './task.repository';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { Task } from './entities/task.entity';
import { List } from '../lists/entities/list.entity';
export declare class TasksService {
    private readonly taskRepository;
    private readonly listRepository;
    constructor(taskRepository: TaskRepository, listRepository: ListRepository);
    create(listId: number, createTaskDto: CreateTaskDto, userId: number): Promise<Task>;
    findAll(listId: number, userId: number): Promise<Task[]>;
    findOne(listId: number, id: number, userId: number): Promise<Task>;
    update(listId: number, id: number, updateTaskDto: UpdateTaskDto, userId: number): Promise<{
        title: string;
        description: string;
        status: import("./entities/task.entity").Status;
        id: number;
        created_at: Date;
        list: List;
    } & Task>;
    remove(listId: number, id: number, userId: number): Promise<number>;
}
