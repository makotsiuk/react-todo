import { Status } from "../entities/task.entity";
export declare class CreateTaskDto {
    title: string;
    description: string;
    status: Status;
}
