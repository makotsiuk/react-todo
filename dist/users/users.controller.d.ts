import { CreateUsertDTO } from './create-user.dto';
import { UsersService } from './users.service';
export declare class UsersController {
    private readonly usersService;
    constructor(usersService: UsersService);
    create(createUsertDTO: CreateUsertDTO): Promise<{
        status: string;
    }>;
}
