import { UserRepository } from './user.repository';
import { User } from './user.entity';
import { CreateUsertDTO } from './create-user.dto';
export declare class UsersService {
    private userRepository;
    constructor(userRepository: UserRepository);
    findOne(email: string): Promise<User | undefined>;
    findAll(): Promise<User[] | undefined>;
    createNew(createUsertDTO: CreateUsertDTO): Promise<{
        status: string;
    }>;
}
