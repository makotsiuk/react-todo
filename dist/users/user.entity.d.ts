import { List } from './../lists/entities/list.entity';
import { BaseEntity } from 'typeorm';
export declare class User extends BaseEntity {
    id: number;
    email: string;
    password: string;
    lists: List[];
    setPassword(password: string): Promise<void>;
}
