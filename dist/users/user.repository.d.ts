import { CreateUsertDTO } from './create-user.dto';
import { Repository } from 'typeorm';
import { User } from './user.entity';
export declare class UserRepository extends Repository<User> {
    constructor();
    createUser(createUserDTO: CreateUsertDTO): Promise<{
        status: string;
    }>;
}
