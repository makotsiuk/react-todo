import { List } from './entities/list.entity';
import { ListRepository } from './list.repository';
import { CreateListDto } from './dto/create-list.dto';
import { UpdateListDto } from './dto/update-list.dto';
import { User } from '../users/user.entity';
export declare class ListsService {
    private readonly listRepository;
    constructor(listRepository: ListRepository);
    create(createListDto: CreateListDto, user: User): Promise<List>;
    findAll(userId: any, offset: any): Promise<[List[], number]>;
    findOne(id: number, userId: number): Promise<List>;
    update(id: number, updateListDto: UpdateListDto, userId: number): Promise<{
        title: string;
        thumbnail: string;
        excerpt: string;
        id: number;
        created_at: Date;
        user: User;
        tasks: import("../tasks/entities/task.entity").Task[];
    } & List>;
    remove(id: number, userId: number): Promise<number>;
}
