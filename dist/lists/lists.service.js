"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListsService = void 0;
const list_entity_1 = require("./entities/list.entity");
const list_repository_1 = require("./list.repository");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
let ListsService = class ListsService {
    constructor(listRepository) {
        this.listRepository = listRepository;
    }
    async create(createListDto, user) {
        const list = new list_entity_1.List();
        list.title = createListDto.title;
        list.thumbnail = createListDto.thumbnail,
            list.excerpt = createListDto.excerpt;
        list.created_at = new Date();
        list.user = user;
        return await this.listRepository.save(list);
    }
    async findAll(userId, offset) {
        const lists = await this.listRepository.findAndCount({
            where: {
                user: {
                    id: userId
                }
            },
            take: 6,
            skip: offset,
            order: {
                created_at: 'DESC'
            }
        });
        return lists;
    }
    async findOne(id, userId) {
        const list = await this.listRepository.findOne(id, {
            where: {
                user: {
                    id: userId
                }
            },
            relations: [
                'tasks'
            ]
        });
        return list;
    }
    async update(id, updateListDto, userId) {
        const list = await this.listRepository.findOne(id, {
            where: {
                user: {
                    id: userId
                }
            }
        });
        if (list) {
            return (await this.listRepository.save(Object.assign(Object.assign({}, list), { title: updateListDto.title, thumbnail: updateListDto.thumbnail, excerpt: updateListDto.excerpt })));
        }
        else {
            throw new common_1.NotFoundException();
        }
    }
    async remove(id, userId) {
        const list = await this.listRepository.findOne(id, {
            where: {
                user: {
                    id: userId
                }
            }
        });
        if (list) {
            return (await this.listRepository.delete(id)).affected;
        }
        else {
            throw new common_1.NotFoundException();
        }
    }
};
ListsService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(list_entity_1.List)),
    __metadata("design:paramtypes", [list_repository_1.ListRepository])
], ListsService);
exports.ListsService = ListsService;
//# sourceMappingURL=lists.service.js.map