"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.List = void 0;
const task_entity_1 = require("./../../tasks/entities/task.entity");
const user_entity_1 = require("./../../users/user.entity");
const typeorm_1 = require("typeorm");
let List = class List extends typeorm_1.BaseEntity {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], List.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], List.prototype, "title", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], List.prototype, "thumbnail", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], List.prototype, "excerpt", void 0);
__decorate([
    typeorm_1.Column({
        type: 'int'
    }),
    __metadata("design:type", Date)
], List.prototype, "created_at", void 0);
__decorate([
    typeorm_1.ManyToOne(() => user_entity_1.User, user => user.lists, {
        onDelete: 'CASCADE'
    }),
    __metadata("design:type", user_entity_1.User)
], List.prototype, "user", void 0);
__decorate([
    typeorm_1.OneToMany(() => task_entity_1.Task, task => task.list, {
        cascade: true
    }),
    __metadata("design:type", Array)
], List.prototype, "tasks", void 0);
List = __decorate([
    typeorm_1.Entity({
        name: 'lists'
    })
], List);
exports.List = List;
//# sourceMappingURL=list.entity.js.map