import { Task } from './../../tasks/entities/task.entity';
import { User } from './../../users/user.entity';
import { BaseEntity } from 'typeorm';
export declare class List extends BaseEntity {
    id: number;
    title: string;
    thumbnail: string;
    excerpt: string;
    created_at: Date;
    user: User;
    tasks: Task[];
}
