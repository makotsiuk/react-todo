import { ListsService } from './lists.service';
import { CreateListDto } from './dto/create-list.dto';
import { UpdateListDto } from './dto/update-list.dto';
export declare class ListsController {
    private readonly listsService;
    constructor(listsService: ListsService);
    create(createListDto: CreateListDto, req: any): Promise<import("./entities/list.entity").List>;
    findAll(offset: string, req: any): Promise<[import("./entities/list.entity").List[], number]>;
    findOne(id: string, req: any): Promise<import("./entities/list.entity").List>;
    update(id: string, updateListDto: UpdateListDto, req: any): Promise<{
        title: string;
        thumbnail: string;
        excerpt: string;
        id: number;
        created_at: Date;
        user: import("../users/user.entity").User;
        tasks: import("../tasks/entities/task.entity").Task[];
    } & import("./entities/list.entity").List>;
    remove(id: string, req: any): Promise<number>;
}
