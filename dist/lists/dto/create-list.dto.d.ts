export declare class CreateListDto {
    title: string;
    thumbnail: string;
    excerpt: string;
}
