"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataSeeder = void 0;
const task_entity_1 = require("./../../tasks/entities/task.entity");
const list_entity_1 = require("./../../lists/entities/list.entity");
const user_entity_1 = require("./../../users/user.entity");
class DataSeeder {
    async run(factory, connection) {
        let counter = 0;
        const users = await factory(user_entity_1.User)()
            .map(async (user) => {
            user.email = `admin${counter ? counter : ''}@admin.com`;
            user.password = 'adminadmin';
            counter += 1;
            return user;
        })
            .createMany(2);
        for (const user of users) {
            const lists = await factory(list_entity_1.List)()
                .createMany(20, { user });
            for (const list of lists) {
                await factory(task_entity_1.Task)().createMany(15, { list });
            }
        }
    }
}
exports.DataSeeder = DataSeeder;
//# sourceMappingURL=main.seeder.js.map