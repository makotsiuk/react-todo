"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_seeding_1 = require("typeorm-seeding");
const user_entity_1 = require("../../users/user.entity");
typeorm_seeding_1.define(user_entity_1.User, (faker) => {
    const user = new user_entity_1.User();
    user.email = faker.internet.email();
    user.password = faker.internet.password();
    return user;
});
//# sourceMappingURL=user.factory.js.map