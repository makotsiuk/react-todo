"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_seeding_1 = require("typeorm-seeding");
const task_entity_1 = require("../../tasks/entities/task.entity");
typeorm_seeding_1.define(task_entity_1.Task, (faker) => {
    const task = new task_entity_1.Task();
    task.title = faker.lorem.sentence();
    task.description = faker.lorem.text();
    task.created_at = faker.date.past();
    task.status = faker.random.number({
        min: 0,
        max: 2
    });
    return task;
});
//# sourceMappingURL=task.factory.js.map