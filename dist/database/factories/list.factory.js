"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_seeding_1 = require("typeorm-seeding");
const list_entity_1 = require("../../lists/entities/list.entity");
typeorm_seeding_1.define(list_entity_1.List, (faker) => {
    const list = new list_entity_1.List();
    list.title = faker.lorem.sentence();
    list.excerpt = faker.lorem.sentence();
    list.thumbnail = faker.image.imageUrl();
    list.created_at = faker.date.past();
    return list;
});
//# sourceMappingURL=list.factory.js.map