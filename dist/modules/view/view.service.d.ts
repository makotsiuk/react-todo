import { OnModuleInit } from '@nestjs/common';
export declare class ViewService implements OnModuleInit {
    private server;
    onModuleInit(): Promise<void>;
    getNextServer(): any;
}
